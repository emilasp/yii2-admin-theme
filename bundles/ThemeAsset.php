<?php
namespace emilasp\admintheme\bundles;

use yii\web\AssetBundle;

/**
 * Class ThemeAsset
 * @package emilasp\admintheme\bundles
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/emilasp/yii2-admin-theme/assets/';
    public $css        = [
        'vendor/wolf-html/css/compiled/theme.css',
        'vendor/wolf-html/css/vendor/brankic.css',
        'vendor/wolf-html/css/vendor/ionicons.min.css',
        'vendor/wolf-html/css/vendor/jquery.dataTables.css',
        'css/theme.css',
        //'css/bs3-paper.css',
    ];
    public $js         = [
        'js/theme.js',
        'vendor/wolf-html/js/theme.js',
        'vendor/wolf-html/js/vendor/jquery.cookie.js',
        'vendor/wolf-html/js/vendor/jquery.dataTables.min.js',
    ];
    public $depends    = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
