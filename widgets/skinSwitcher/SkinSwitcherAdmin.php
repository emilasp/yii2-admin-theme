<?php
namespace emilasp\admintheme\widgets\skinSwitcher;

use yii\base\Widget;

/**
 * Class SkinSwitcherAdmin
 * @package emilasp\admintheme\widgets\skinSwitcher
 */
class SkinSwitcherAdmin extends Widget
{
    public function run()
    {
        echo $this->render('switcher');
    }
}
