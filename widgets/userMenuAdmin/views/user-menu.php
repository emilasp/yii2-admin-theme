<?php
use emilasp\media\models\File;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>
<div class="current-user">
    <a href="index.html" class="name">

        <?php $file = Yii::$app->user->identity->profile->image ??
            new File(['type' => File::TYPE_FILE_IMAGE]); ?>
        <img class="avatar" src="<?= $file->getUrl(File::SIZE_ICO) ?>"/>
        <span>
            <?= Yii::$app->user->identity->username ?><i class="fa fa-chevron-down"></i>
        </span>

    </a>
    <ul class="menu">
        <li>
            <a href="<?= Url::toRoute(['/users/manage/update', 'id' => Yii::$app->user->id]) ?>">
                <?= Yii::t('users', 'Profile') ?>
            </a>
        </li>
        <li><a href="account-billing.html">Billing</a></li>
        <li><a href="account-notifications.html">Notifications</a></li>
        <li><a href="account-support.html">Help / Support</a></li>
        <li><a href="signup.html">Sign out</a></li>
    </ul>
</div>

