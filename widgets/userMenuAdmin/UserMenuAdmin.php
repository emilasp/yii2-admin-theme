<?php
namespace emilasp\admintheme\widgets\userMenuAdmin;

use yii\base\Widget;

/**
 * Class UserMenuAdmin
 * @package emilasp\admintheme\widgets\userMenuAdmin
 */
class UserMenuAdmin extends Widget
{
    public function run()
    {
        if (!\Yii::$app->user->isGuest) {
            echo $this->render('user-menu');
        }
    }
}
