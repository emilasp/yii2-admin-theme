<div class="metrics clearfix">
    <?php use dosamigos\chartjs\ChartJs;

    foreach ($metrics as $name => $value) : ?>
        <div class="metric">
            <span class="field"><?= Yii::t('im', $name) ?></span>
            <span class="data"><?= $value ?></span>
        </div>
    <?php endforeach ?>

</div>

<div class="dash-chart">
    <h2><?= Yii::t('im', 'Statistics of day') ?></h2>
    <?= ChartJs::widget([
        'type' => 'Line',
        'options' => [
            'height' => 400,
        ],
        'data' => [
            'labels' => $labelsChartDay,
            'datasets' => [
                [
                    'fillColor' => "rgba(220,220,220,0.5)",
                    'strokeColor' => "rgba(220,220,220,1)",
                    'pointColor' => "rgba(220,220,220,1)",
                    'pointStrokeColor' => "#fff",
                    'data' => $dataChartDayCount
                ],
                [
                    'fillColor' => "rgba(151,187,205,0.5)",
                    'strokeColor' => "rgba(151,187,205,1)",
                    'pointColor' => "rgba(151,187,205,1)",
                    'pointStrokeColor' => "#fff",
                    'data' => $dataChartDaySum
                ]
            ]
        ]
    ]);
    ?>
    
    
    <h2><?= Yii::t('im', 'All Statistics') ?></h2>
    <?= ChartJs::widget([
        'type' => 'Line',
        'options' => [
            'height' => 400,
        ],
        'data' => [
            'labels' => $labelsChartAll,
            'datasets' => [
                [
                    'fillColor' => "rgba(220,220,220,0.5)",
                    'strokeColor' => "rgba(220,220,220,1)",
                    'pointColor' => "rgba(220,220,220,1)",
                    'pointStrokeColor' => "#fff",
                    'data' => $dataChartAllCount
                ],
                [
                    'fillColor' => "rgba(151,187,205,0.5)",
                    'strokeColor' => "rgba(151,187,205,1)",
                    'pointColor' => "rgba(151,187,205,1)",
                    'pointStrokeColor' => "#fff",
                    'data' => $dataChartAllSum
                ]
            ]
        ]
    ]);
    ?>
</div>
