<?php
namespace emilasp\admintheme\widgets\metrics;

use emilasp\im\common\models\Client;
use emilasp\im\common\models\Order;
use yii\base\Widget;

/**
 * Class MetricsAdmin
 * @package emilasp\admintheme\widgets\metrics
 */
class MetricsAdmin extends Widget
{
    public $metrics;

    private $orderCount = 0;
    private $orderSum   = 0;

    private $dayOrderCount = 0;
    private $dayOrderSum   = 0;

    private $dataChartAll      = [];
    private $dataChartAllCount = [];
    private $dataChartAllSum   = [];
    private $labelsChartAll    = [];

    private $dataChartDay      = [];
    private $dataChartDayCount = [];
    private $dataChartDaySum   = [];
    private $labelsChartDay    = [];
    


    public function init()
    {
        parent::init();

        $this->setStatAll();
        $this->setStatOfDay();
        $this->getMetrics();
    }

    public function run()
    {
        echo $this->render('metrics', [
            'metrics' => $this->metrics,

            'labelsChartAll'    => $this->labelsChartAll,
            'dataChartAllCount' => $this->dataChartAllCount,
            'dataChartAllSum'   => $this->dataChartAllSum,

            'labelsChartDay'    => $this->labelsChartDay,
            'dataChartDayCount' => $this->dataChartDayCount,
            'dataChartDaySum'   => $this->dataChartDaySum,
        ]);
    }

    /**
     * Set All counts
     */
    private function getMetrics()
    {
        $clientsCount = Client::find()->where(['status' => Client::STATUS_ENABLED])->count();

        $this->metrics = [
            'Clients'       => $clientsCount,
            'Orders of day' => count($this->dayOrderCount),
            'Orders all'    => $this->orderCount,
            'Summ day'      => $this->dayOrderSum . \Yii::t('im', 'Rub'),
        ];
    }

    /**
     * Set All stats
     */
    private function setStatAll()
    {
        $orders = Order::find()->where(['>', 'created_at', date('Y-01-01 00:00:00')])->all();

        for ($i = 1; $i < 13; $i++) {
            $month = str_pad($i, 2, '0', STR_PAD_LEFT);

            $this->labelsChartAll[] = $month;

            $this->dataChartAll[$month]['count'] = 0;
            $this->dataChartAll[$month]['sum']   = 0;
        }

        foreach ($orders as $order) {
            $this->orderSum += $order->sum;
            $this->orderCount++;

            $month = date('m', strtotime($order->created_at));

            $this->dataChartAll[$month]['sum'] += $order->sum;
            $this->dataChartAll[$month]['count']++;
        }

        foreach ($this->dataChartAll as $data) {
            $this->dataChartAllSum[]   = $data['sum'];
            $this->dataChartAllCount[] = $data['count'];
        }
    }

    private function setStatOfDay()
    {
        $ordersOfDay = Order::findBySql('SELECT * FROM im_order WHERE DATE(created_at) = DATE(NOW())')->all();

        for ($i = 0; $i < 24; $i++) {
            $hour = str_pad($i, 2, '0', STR_PAD_LEFT);

            $this->labelsChartDay[] = $hour;

            $this->dataChartDay[$hour]['count'] = 0;
            $this->dataChartDay[$hour]['sum']   = 0;
        }

        foreach ($ordersOfDay as $order) {
            $this->dayOrderSum += $order->sum;
            $this->dayOrderCount++;

            $hour = date('H', strtotime($order->created_at));

            $this->dataChartDay[$hour]['sum'] += $order->sum;
            $this->dataChartDay[$hour]['count']++;
        }

        foreach ($this->dataChartDay as $data) {
            $this->dataChartDaySum[]   = $data['sum'];
            $this->dataChartDayCount[] = $data['count'];
        }
    }
}
